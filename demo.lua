-- LUALOCALS < ---------------------------------------------------------
local math, minetest
    = math, minetest
local math_random
    = math.random
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

local blackdot = "[combine:1x1^[noalpha"
local bdesc = blackdot:gsub("%^", "\\^"):gsub(":", "\\:")
local frame = "[combine:16x16:0,0=" .. bdesc .. ":15,0=" .. bdesc
.. ":0,15=" .. bdesc .. ":15,15=" .. bdesc

local function regdemo(num, desc, func)
	return minetest.register_node(modname .. ":demo" .. num, {
			description = desc,
			drawtype = "allfaces",
			tiles = {{name = frame, backface_culling = false}},
			groups = {[modname] = 1},
			paramtype = "light",
			sunlight_propagates = true,
			on_construct = function(pos)
				return minetest.get_meta(pos):set_string("infotext", desc)
			end,
			on_punch = function(pos) return minetest.remove_node(pos) end,
			on_demo = func
		})
end

minetest.register_abm({
		nodenames = {"group:" .. modname},
		interval = 2,
		chance = 1,
		action = function(pos, node)
			local def = minetest.registered_nodes[node.name]
			if def and def.on_demo then return def.on_demo(pos, node) end
		end
	})

regdemo(1, "ASMD ShockRifle Effect", function(pos)
		local endpos = {
			x = pos.x + math_random() * 20 - 10,
			y = pos.y + math_random() * 20 - 10,
			z = pos.z + math_random() * 20 - 10,
		}
		minetest.add_particlespawner_advanced({
				amount = 25,
				time = 0.25,
				pos = {
					x = {pos.x, endpos.x - pos.x},
					y = {pos.y, endpos.y - pos.y},
					z = {pos.z, endpos.z - pos.z}
				},
				velocity = {
					x = {(endpos.x - pos.x) / 2, -(endpos.x - pos.x) / 2},
					y = {(endpos.y - pos.y) / 2, -(endpos.y - pos.y) / 2},
					z = {(endpos.z - pos.z) / 2, -(endpos.z - pos.z) / 2}
				},
				expirationtime = {2, -0.25},
				texture = blackdot,
				size = {1, 4}
			})
	end)

regdemo(2, "Implosion / Teleport Arrive", function(pos)
		minetest.add_particlespawner_advanced({
				amount = 100,
				time = 0,
				pos = {
					x = {pos.x, -10, 20},
					y = {pos.y, -10, 0, 20},
					z = {pos.z, -10, 0, 0, 20}
				},
				velocity = {
					x = {0, 20, -40},
					y = {0, 20, 0, -40},
					z = {0, 20, 0, 0, -40}
				},
				acceleration = {
					x = {0, -20, 40},
					y = {0, -20, 0, 40},
					z = {0, -20, 0, 0, 40}
				},
				expirationtime = {1},
				texture = blackdot,
				size = {1, 0, 0, 0, 0, 4}
			})
	end)

regdemo(3, "Tornado", function(pos)
		minetest.add_particlespawner_advanced({
				amount = 200,
				time = 2,
				pos = {
					x = {pos.x - 10, 0, 20},
					y = {pos.y, 0, 0, 10},
					z = {pos.z - 10, 0, 0, 0, 20}
				},
				velocity = {
					x = {-5, 0, 0, 0, 10},
					y = {0, 0, 0, 10},
					z = {5, 0, -10}
				},
				acceleration = {
					x = {2.5, 0, -5},
					y = {5},
					z = {2.5, 0, 0, 0, -5}
				},
				expirationtime = {2},
				texture = blackdot,
				size = {1, 0, 0, 0, 0, 4}
			})
	end)
